#include "constants.h"
#include "types.h"

#include "trading/quantity.h"
#include "trading/types.h"

// See here for some discussion of quantisation and resolution:
// https://docs.starkware.co/starkex-v3/starkex-deep-dive/starkex-specific-concepts#quantization-and-resolution
//
// In stark, each asset is given a particular resolution and the functions below
// convert in to that resolution for use in stark signatures
//
// "As an example, say that the resolution of ETH is 10,000,000. This means that
// 10,000,000 tokens in our system represent 1ETH, therefore 1 in the system is
// 10^-7 ETH."

namespace Kenetic::EsV2::Dydx::Stark::Quantum {

inline Cost toQuantumsRoundUp(const Cost &amount, double assetResolution) {
  auto quantized = Cost::fromDouble(
      std::ceil((amount.toDouble() * assetResolution) - 0.001));
  return quantized;
}

inline Cost toQuantumsRoundDown(const Cost &amount, double assetResolution) {
  auto quantized = Cost::fromDouble(
      std::floor((amount.toDouble() * assetResolution) + 0.001));
  return quantized;
}

inline Cost toQuantumsExact(const Cost &amount, double assetResolution) {
  return Cost::fromDouble(amount.toDouble() * assetResolution);
}

inline Cost getFeeQuantums(const Cost &collateralQuantums,
                           const Fee &limitFee) {
  return toQuantumsRoundUp(collateralQuantums, limitFee.toDouble());
}

inline Cost getSyntheticQuantums(const Trading::NativeSymbol &market,
                                 const Trading::Quantity &qty) {
  auto it = gAssetResolutionMap.find(market);
  assert(it != gAssetResolutionMap.end());
  return toQuantumsExact(Cost::fromDouble(qty.toDouble()),
                         it->second.toDouble());
}

inline Cost getCollateralQuantums(Trading::Side side,
                                  const Trading::Price &price,
                                  const Trading::Quantity &qty) {
  Cost collateralCost = Cost::fromDouble(price.toDouble() * qty.toDouble());
  if (side == Trading::Side::Buy) {
    return toQuantumsRoundUp(collateralCost, ASSET_USDC_RESOLUTION.toDouble());
  } else {
    return toQuantumsRoundDown(collateralCost,
                               ASSET_USDC_RESOLUTION.toDouble());
  }
}

} // namespace Kenetic::EsV2::Dydx::Stark::Quantum
