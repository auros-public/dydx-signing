#pragma once

#include "common/decimal.h"

#include <openssl/bn.h>

namespace Kenetic::EsV2::Dydx::Stark {

using Cost = Common::Decimal<struct CostTag>;
using Fee = Common::Decimal<struct FeeTag>;

// a openssl BIGNUM wrappper for formatter and RAII
struct BigInt {
  BigInt() { mData = BN_new(); }

  BigInt(BIGNUM *data) : mData(data) {}

  BigInt(const BigInt &other) { mData = BN_dup(other.mData); }

  BigInt(BigInt &&other) noexcept {
    mData = other.mData;
    other.mData = nullptr;
  }

  BigInt &operator=(const BigInt &other) {
    if (this == &other)
      return *this;

    if (mData) {
      BN_free(mData);
    }

    mData = BN_dup(other.mData);

    return *this;
  }

  BigInt &operator=(BigInt &&other) {
    if (this == &other)
      return *this;

    if (mData) {
      BN_free(mData);
    }

    mData = other.mData;
    other.mData = nullptr;

    return *this;
  }

  ~BigInt() { BN_free(mData); }

  static BigInt fromHexString(const char *cstr) {
    BigInt res;
    BN_hex2bn(&res.mData, cstr);
    return res;
  }

  BIGNUM *mData = nullptr;
};

} // namespace Kenetic::EsV2::Dydx::Stark

template <> struct fmt::formatter<Kenetic::EsV2::Dydx::Stark::BigInt> {
  template <typename ParseContext> constexpr auto parse(ParseContext &ctx) {
    return ctx.begin();
  }

  template <typename FormatContext>
  auto format(const Kenetic::EsV2::Dydx::Stark::BigInt &bi,
              FormatContext &ctx) {
    auto *cstr = BN_bn2hex(bi.mData);
    auto len = strlen(cstr);
    assert(len % 2 == 0);
    for (auto i = len; i < 64ul; ++i) {
      fmt::format_to(ctx.out(), "0");
    }
    auto it = fmt::format_to(ctx.out(), "{}", cstr);
    OPENSSL_free(cstr);
    return it;
  }
};
