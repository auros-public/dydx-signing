// TODO: Replace functions deprecated in openssl v3.x
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include "auth.h"
#include "elliptic_curve_constants.h"
#include "quantum.h"

#include "common/hash_sha.h"
#include "common/logger.h"

#include <openssl/ecdsa.h>
#include <openssl/rand.h>
#include <openssl/sha.h>

#include <cctype>

namespace Kenetic::EsV2::Dydx::Stark {

using namespace Kenetic::Common;
using namespace Kenetic::Trading;
using namespace std::literals;

namespace {

uint64_t getNonceFromOrderId(OrderId orderId) {
  auto str = fmt::format("{}", orderId);
  auto digest32 = Common::computeHashSha256(Common::makeByteSpan(str));
  BigInt nonce = BigInt::fromHexString(fmt::format("{}", digest32).c_str());
  return BN_mod_word(nonce.mData, NONCE_UPPER_BOUND_EXCLUSIVE);
}

BigInt packMessagePartOne(uint64_t quantumsAmountSell,
                          uint64_t quantumsAmountBuy,
                          uint64_t quantumsAmountFee, uint64_t nonce) {

  BigInt part1;
  auto &msg = part1.mData;

  BN_set_word(msg, 0);

  // For the shift functions, r and a may be the same variable.

  BN_lshift(msg, msg, AMOUNT_FIELD_BITS);
  BN_add_word(msg, quantumsAmountSell);

  BN_lshift(msg, msg, AMOUNT_FIELD_BITS);
  BN_add_word(msg, quantumsAmountBuy);

  BN_lshift(msg, msg, AMOUNT_FIELD_BITS);
  BN_add_word(msg, quantumsAmountFee);

  BN_lshift(msg, msg, NONCE_FIELD_BITS);
  BN_add_word(msg, nonce);

  return part1;
}

BigInt packMessagePartTwo(uint64_t vaultId, uint64_t expirationEpochHours) {

  BigInt part2;
  auto &msg = part2.mData;

  BN_set_word(msg, LIMIT_ORDER_TYPE);

  for (auto i = 0; i < 3; ++i) {
    BN_lshift(msg, msg, VAULT_ID_FIELD_BITS);
    BN_add_word(msg, vaultId);
  }

  BN_lshift(msg, msg, TIMESTAMP_FIELD_BITS);
  BN_add_word(msg, expirationEpochHours);

  BN_lshift(msg, msg, PADDING_FIELD_BITS);

  return part2;
}
} // namespace

Auth::Auth(ILoggerFactory &lf, uint64_t networkId, uint64_t positionId,
           const std::string &starkPrivateKey)
    : mLogger(lf.makeLogger("Dydx:Stark:Auth")), mNetworkId(networkId),
      mPositionId(positionId) {
  if (mNetworkId != MAINNET_NETWORK_ID && mNetworkId != ROPSTEN_NETWORK_ID)
    throw std::logic_error(fmt::format("unknown network id {}", mNetworkId));

  auto sv = std::string_view(starkPrivateKey);
  if (sv[0] != '0' || std::tolower(sv[1]) != 'x') {
    throw std::runtime_error("only hex string is supported");
  }

  if (std::any_of(sv.begin() + 2, sv.end(),
                  [](unsigned char c) { return !std::isxdigit(c); })) {
    throw std::runtime_error("non hexadecimal character found");
  }

  mPrivateKey = BigInt::fromHexString(starkPrivateKey.c_str() + 2);

  createEllipticCurve();

  loadConstantPoints();

  LOG_DEBUG(mLogger, "Created: network id={}, position id={}", mNetworkId,
            mPositionId);
}

Auth::~Auth() {
  if (mCtx) {
    BN_CTX_free(mCtx);
  }

  if (mEcGroup) {
    EC_GROUP_free(mEcGroup);
  }

  if (mEcKey) {
    EC_KEY_free(mEcKey);
  }

  for (auto *p : mConstantPoints) {
    if (p) {
      EC_POINT_free(p);
    }
  }
}

void Auth::createEllipticCurve() {
  // TODO error handlings

  mCtx = BN_CTX_new();

  mEcGroup = EC_GROUP_new_curve_GFp(EC_FIELD_PRIME.mData, EC_ALPHA.mData,
                                    EC_BETA.mData, nullptr);
  EC_POINT *point = EC_POINT_new(mEcGroup);
  EC_POINT_set_affine_coordinates_GFp(mEcGroup, point, EC_GEN_X.mData,
                                      EC_GEN_Y.mData, nullptr);
  EC_GROUP_set_generator(mEcGroup, point, EC_ORDER.mData, nullptr);
  EC_POINT_free(point);

  mEcKey = EC_KEY_new();
  EC_KEY_set_group(mEcKey, mEcGroup);

  EC_KEY_set_private_key(mEcKey, mPrivateKey.mData);

  EC_POINT *publicPoint = EC_POINT_new(mEcGroup);
  EC_POINT_mul(mEcGroup, publicPoint, mPrivateKey.mData, nullptr, nullptr,
               nullptr);
  EC_KEY_set_public_key(mEcKey, publicPoint);
  EC_POINT_free(publicPoint);
}

void Auth::loadConstantPoints() {
  for (auto &p : EC_POINTS) {
    EC_POINT *point = EC_POINT_new(mEcGroup);

    BigInt x = BigInt::fromHexString(p.first.c_str());
    BigInt y = BigInt::fromHexString(p.second.c_str());

    EC_POINT_set_affine_coordinates_GFp(mEcGroup, point, x.mData, y.mData,
                                        nullptr);
    mConstantPoints.push_back(point);
  }

  LOG_DEBUG(mLogger, "Loaded {} constant points", mConstantPoints.size());
}

std::string Auth::sign(const NativeSymbol &market, OrderId orderId,
                       const Price &price, const Quantity &qty, Side side,
                       const Fee &limitFee,
                       TimestampNs expirationTimestamp) const {
  Cost syntheticQuantums = Quantum::getSyntheticQuantums(market, qty);
  Cost collateralQuantums = Quantum::getCollateralQuantums(side, price, qty);
  Cost feeQuantums = Quantum::getFeeQuantums(collateralQuantums, limitFee);

  auto it = gAssetIdMap.find(market);
  assert(it != gAssetIdMap.end());
  auto &assetId = it->second;

  if (side == Side::Buy) {
    return doSign(
        orderId,
        (mNetworkId == MAINNET_NETWORK_ID) ? MAINNET_COLLATERAL_ASSET_ID
                                           : ROPSTEN_COLLATERAL_ASSET_ID,
        collateralQuantums, assetId, syntheticQuantums,
        (mNetworkId == MAINNET_NETWORK_ID) ? MAINNET_COLLATERAL_ASSET_ID
                                           : ROPSTEN_COLLATERAL_ASSET_ID,
        feeQuantums, expirationTimestamp);
  } else {
    return doSign(
        orderId, assetId, syntheticQuantums,
        (mNetworkId == MAINNET_NETWORK_ID) ? MAINNET_COLLATERAL_ASSET_ID
                                           : ROPSTEN_COLLATERAL_ASSET_ID,
        collateralQuantums,
        (mNetworkId == MAINNET_NETWORK_ID) ? MAINNET_COLLATERAL_ASSET_ID
                                           : ROPSTEN_COLLATERAL_ASSET_ID,
        feeQuantums, expirationTimestamp);
  }
}

BigInt Auth::calculatePedersenHash(const BigInt &first,
                                   const BigInt &second) const {
  auto *point = EC_POINT_dup(mConstantPoints[0], mEcGroup);

  auto i = 2ul;
  // EC_POINT *res = EC_POINT_new(group);

  for (; i < 254; ++i) {
    auto *p = mConstantPoints[i];
    if (BN_is_bit_set(first.mData, i - 2)) {
      EC_POINT_add(mEcGroup, point, point, p, mCtx);
      // EC_POINT_copy(point, res);
    }
  }

  for (; i < 506; ++i) {
    auto *p = mConstantPoints[i];
    if (BN_is_bit_set(second.mData, i - 254)) {
      EC_POINT_add(mEcGroup, point, point, p, mCtx);
      // EC_POINT_copy(point, res);
    }
  }

  BigInt x;
  EC_POINT_get_affine_coordinates_GFp(mEcGroup, point, x.mData, nullptr, mCtx);
  EC_POINT_free(point);
  return x;
}

std::string Auth::doSign(OrderId orderId, const BigInt &assetIdSell,
                         const Cost &quantumsAmountSell,
                         const BigInt &assetIdBuy,
                         const Cost &quantumsAmountBuy,
                         const BigInt &assetIdFee,
                         const Cost &quantumsAmountFee,
                         TimestampNs expirationTimestamp) const {

  // Orders may have a short time-to-live on the orderbook, but we need
  // to ensure their signatures are valid by the time they reach the
  // blockchain. Therefore, we enforce that the signed expiration includes
  // a buffer relative to the expiration timestamp sent to the dYdX API.
  //
  // Add 1 so that times on the hour are rounded up.
  uint64_t expirationEpochHours =
      std::ceil((1.0 + expirationTimestamp.toSecond()) / 3600.0) +
      ORDER_SIGNATURE_EXPIRATION_BUFFER_HOURS;

  uint64_t nonce = getNonceFromOrderId(orderId);

  auto part1 =
      packMessagePartOne(quantumsAmountSell.toInt(), quantumsAmountBuy.toInt(),
                         quantumsAmountFee.toInt(), nonce);
  auto part2 = packMessagePartTwo(mPositionId, expirationEpochHours);

  auto h1 = calculatePedersenHash(assetIdSell, assetIdBuy);
  auto h2 = calculatePedersenHash(h1, assetIdFee);
  auto h3 = calculatePedersenHash(h2, part1);
  auto hash = calculatePedersenHash(h3, part2);

  LOG_TRACE(
      mLogger,
      "assetIdSell: {}, quantumsAmountSell: {}, assetIdBuy: {}, "
      "quantumsAmountBuy: {}, assetIdFee: {}, quantumsAmountFee: {}, nonce: "
      "{}, expirationEpochHours: {}, part1: {}, part2: {}, hash: {}",
      assetIdSell, quantumsAmountSell.toInt(), assetIdBuy,
      quantumsAmountBuy.toInt(), assetIdFee, quantumsAmountFee.toInt(), nonce,
      expirationEpochHours, part1, part2, hash);

  if (BN_is_zero(hash.mData)) {
    throw std::runtime_error("hash cannot be zero");
  }

  if (BN_cmp(hash.mData, EC_UPPER_BOUND.mData) >= 0) {
    LOG_WARN(mLogger, "Hash too big {}", hash);
    // I'm not sure what to do here.
    // openssl shifts the hash and only uses the top however many bits
    // but I think that is one of the reasons openssl isn't working for this.
    // The starkware lib just asserts.
    // And the python dydx lib throws saying it is unsignable.
    // I haven't seen this happen yet.
    throw std::runtime_error(fmt::format("hash too big {}", hash));
  }

  BigInt r;
  BigInt kinv(nullptr);

  // Chance of failure minimal. I haven't seen it happen in 10,000
  // orders.
  for (int i = 0; i < 2; i++) {
#if 0
    ECDSA_sign_setup(mEcKey, nullptr, &kinv.mData, &r.mData);
#else
    BigInt k;
    BN_rand(k.mData, 256, BN_RAND_TOP_ANY, BN_RAND_BOTTOM_ANY);

    if (BN_is_zero(k.mData)) {
      LOG_WARN(mLogger, "Bad k value");
      continue;
    }

    // caculate k^-1
    kinv = BigInt();
    BN_mod_inverse(kinv.mData, k.mData, EC_ORDER.mData, mCtx);

    // Calculate the random point R = k * G and take its x-coordinate: r = R.x
    EC_POINT *point = EC_POINT_new(mEcGroup);
    EC_POINT_mul(mEcGroup, point, k.mData, nullptr, nullptr, mCtx);
    EC_POINT_get_affine_coordinates_GFp(mEcGroup, point, r.mData, nullptr,
                                        mCtx);
    EC_POINT_free(point);

    if (BN_is_zero(r.mData) || BN_cmp(r.mData, EC_ORDER.mData) >= 0) {
      LOG_WARN(mLogger, "Bad r value");
      continue;
    }
#endif

    // Calculate the signature proof: s = k^-1 * (hash + r * private_key) mod n
    BigInt rp;
    BN_mod_mul(rp.mData, r.mData, mPrivateKey.mData, EC_ORDER.mData, mCtx);

    BigInt temp;
    BN_add(temp.mData, rp.mData, hash.mData);

    BigInt s;
    BN_mod_mul(s.mData, temp.mData, kinv.mData, EC_ORDER.mData, mCtx);

    if (BN_is_zero(s.mData)) {
      LOG_WARN(mLogger, "Bad s value");
      continue;
    }

    LOG_TRACE(mLogger, "kinv={}, r={}, s={}", kinv, r, s);

    return fmt::format("{}{}", r, s);
  }

  throw std::runtime_error("Could not generate valid signature");
}

} // namespace Kenetic::EsV2::Dydx::Stark
#pragma GCC diagnostic pop
