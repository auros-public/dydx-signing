#pragma once

#include "types.h"

#include "common/logging.h"
#include "common/timestampns.h"

#include "trading/price.h"
#include "trading/quantity.h"
#include "trading/types.h"

#include <openssl/ec.h>

namespace Kenetic::EsV2::Dydx::Stark {

class Auth {
public:
  Auth(Common::ILoggerFactory &, uint64_t networkId, uint64_t positionId,
       const std::string &starkPrivateKey);

  ~Auth();

  std::string sign(const Trading::NativeSymbol &, Trading::OrderId,
                   const Trading::Price &, const Trading::Quantity &,
                   Trading::Side, const Fee &limitFee,
                   Common::TimestampNs expirationTimestamp) const;

private:
  void createEllipticCurve();
  void loadConstantPoints();
  BigInt calculatePedersenHash(const BigInt &first, const BigInt &second) const;

  std::string doSign(Trading::OrderId, const BigInt &assetIdSell,
                     const Cost &quantumsAmountSell, const BigInt &assetIdBuy,
                     const Cost &quantumsAmountBuy, const BigInt &assetIdFee,
                     const Cost &quantumsAmountFee,
                     Common::TimestampNs expirationTimestamp) const;

  Common::LoggerPtr mLogger;
  uint64_t mNetworkId;
  uint64_t mPositionId;
  std::string mStarkPrivateKey;

  BN_CTX *mCtx = nullptr;
  EC_GROUP *mEcGroup = nullptr;
  EC_KEY *mEcKey = nullptr;
  BigInt mPrivateKey;

  std::vector<EC_POINT *> mConstantPoints;
};

} // namespace Kenetic::EsV2::Dydx::Stark
