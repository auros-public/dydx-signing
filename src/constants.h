#pragma once

#include "types.h"

#include "trading/types.h"

namespace Kenetic::EsV2::Dydx::Stark {

constexpr uint64_t MAINNET_NETWORK_ID = 1;
constexpr uint64_t ROPSTEN_NETWORK_ID = 3;

constexpr auto MARKET_BTC_USD = "BTC-USD";
constexpr auto MARKET_ETH_USD = "ETH-USD";
constexpr auto MARKET_LINK_USD = "LINK-USD";
constexpr auto MARKET_AAVE_USD = "AAVE-USD";
constexpr auto MARKET_UNI_USD = "UNI-USD";
constexpr auto MARKET_SUSHI_USD = "SUSHI-USD";
constexpr auto MARKET_SOL_USD = "SOL-USD";
constexpr auto MARKET_YFI_USD = "YFI-USD";
constexpr auto MARKET_1INCH_USD = "1INCH-USD";
constexpr auto MARKET_AVAX_USD = "AVAX-USD";
constexpr auto MARKET_SNX_USD = "SNX-USD";
constexpr auto MARKET_CRV_USD = "CRV-USD";
constexpr auto MARKET_UMA_USD = "UMA-USD";
constexpr auto MARKET_DOT_USD = "DOT-USD";
constexpr auto MARKET_DOGE_USD = "DOGE-USD";
constexpr auto MARKET_MATIC_USD = "MATIC-USD";
constexpr auto MARKET_MKR_USD = "MKR-USD";
constexpr auto MARKET_FIL_USD = "FIL-USD";
constexpr auto MARKET_ADA_USD = "ADA-USD";
constexpr auto MARKET_ATOM_USD = "ATOM-USD";
constexpr auto MARKET_COMP_USD = "COMP-USD";
constexpr auto MARKET_BCH_USD = "BCH-USD";
constexpr auto MARKET_LTC_USD = "LTC-USD";
constexpr auto MARKET_EOS_USD = "EOS-USD";
constexpr auto MARKET_ALGO_USD = "ALGO-USD";
constexpr auto MARKET_ZRX_USD = "ZRX-USD";
constexpr auto MARKET_XMR_USD = "XMR-USD";
constexpr auto MARKET_ZEC_USD = "ZEC-USD";
constexpr auto MARKET_ENJ_USD = "ENJ-USD";
constexpr auto MARKET_XLM_USD = "XLM-USD";
constexpr auto MARKET_ETC_USD = "ETC-USD";
constexpr auto MARKET_NEAR_USD = "NEAR-USD";
constexpr auto MARKET_LUNA_USD = "LUNA-USD";
constexpr auto MARKET_RUNE_USD = "RUNE-USD";
constexpr auto MARKET_TRX_USD = "TRX-USD";
constexpr auto MARKET_XTZ_USD = "XTZ-USD";
constexpr auto MARKET_HNT_USD = "HNT-USD";
constexpr auto MARKET_ICP_USD = "ICP-USD";
constexpr auto MARKET_AR_USD = "AR-USD";
constexpr auto MARKET_FLOW_USD = "FLOW-USD";
constexpr auto MARKET_PERP_USD = "PERP-USD";
constexpr auto MARKET_REN_USD = "REN-USD";
constexpr auto MARKET_CELO_USD = "CELO-USD";
constexpr auto MARKET_KSM_USD = "KSM-USD";
constexpr auto MARKET_BAL_USD = "BAL-USD";
constexpr auto MARKET_BNT_USD = "BNT-USD";
constexpr auto MARKET_MIR_USD = "MIR-USD";
constexpr auto MARKET_SRM_USD = "SRM-USD";
constexpr auto MARKET_LON_USD = "LON-USD";
constexpr auto MARKET_DODO_USD = "DODO-USD";
constexpr auto MARKET_ALPHA_USD = "ALPHA-USD";
constexpr auto MARKET_WNXM_USD = "WNXM-USD";
constexpr auto MARKET_XCH_USD = "XCH-USD";

// ------------ Asset Resolution (Quantum Size) ------------
//
// The asset resolution is the number of quantums (Starkware units) that fit
// within one "human-readable" unit of the asset. For example, if the asset
// resolution for BTC is 1e10, then the smallest unit representable within
// Starkware is 1e-10 BTC, i.e. 1/100th of a satoshi.
//
// For the collateral asset (USDC), the chosen resolution corresponds to the
// base units of the ERC-20 token. For the other, synthetic, assets, the
// resolutions are chosen such that prices relative to USDC are close to one.

const Cost ASSET_USDC_RESOLUTION = Cost::fromDouble(1E6);
const std::unordered_map<Trading::NativeSymbol, Cost> gAssetResolutionMap = {
    {MARKET_BTC_USD, Cost::fromDouble(1E10)},
    {MARKET_ETH_USD, Cost::fromDouble(1E9)},
    {MARKET_LINK_USD, Cost::fromDouble(1E7)},
    {MARKET_AAVE_USD, Cost::fromDouble(1E8)},
    {MARKET_UNI_USD, Cost::fromDouble(1E7)},
    {MARKET_SUSHI_USD, Cost::fromDouble(1E7)},
    {MARKET_SOL_USD, Cost::fromDouble(1E7)},
    {MARKET_YFI_USD, Cost::fromDouble(1E10)},
    {MARKET_1INCH_USD, Cost::fromDouble(1E7)},
    {MARKET_AVAX_USD, Cost::fromDouble(1E7)},
    {MARKET_SNX_USD, Cost::fromDouble(1E7)},
    {MARKET_CRV_USD, Cost::fromDouble(1E6)},
    {MARKET_UMA_USD, Cost::fromDouble(1E7)},
    {MARKET_DOT_USD, Cost::fromDouble(1E7)},
    {MARKET_DOGE_USD, Cost::fromDouble(1E5)},
    {MARKET_MATIC_USD, Cost::fromDouble(1E6)},
    {MARKET_MKR_USD, Cost::fromDouble(1E9)},
    {MARKET_FIL_USD, Cost::fromDouble(1E7)},
    {MARKET_ADA_USD, Cost::fromDouble(1E6)},
    {MARKET_ATOM_USD, Cost::fromDouble(1E7)},
    {MARKET_COMP_USD, Cost::fromDouble(1E8)},
    {MARKET_BCH_USD, Cost::fromDouble(1E8)},
    {MARKET_LTC_USD, Cost::fromDouble(1E8)},
    {MARKET_EOS_USD, Cost::fromDouble(1E6)},
    {MARKET_ALGO_USD, Cost::fromDouble(1E6)},
    {MARKET_ZRX_USD, Cost::fromDouble(1E6)},
    {MARKET_XMR_USD, Cost::fromDouble(1E8)},
    {MARKET_ZEC_USD, Cost::fromDouble(1E8)},
    {MARKET_ENJ_USD, Cost::fromDouble(1E6)},
    {MARKET_XLM_USD, Cost::fromDouble(1E5)},
    {MARKET_ETC_USD, Cost::fromDouble(1E7)},
    {MARKET_NEAR_USD, Cost::fromDouble(1E6)},
    {MARKET_LUNA_USD, Cost::fromDouble(1E6)},
    {MARKET_RUNE_USD, Cost::fromDouble(1E6)},
    {MARKET_TRX_USD, Cost::fromDouble(1E4)},
    {MARKET_XTZ_USD, Cost::fromDouble(1E6)},
    {MARKET_HNT_USD, Cost::fromDouble(1E7)},
    {MARKET_ICP_USD, Cost::fromDouble(1E7)},
    {MARKET_AR_USD, Cost::fromDouble(1E7)},
    {MARKET_FLOW_USD, Cost::fromDouble(1E7)},
    {MARKET_PERP_USD, Cost::fromDouble(1E6)},
    {MARKET_REN_USD, Cost::fromDouble(1e5)},
    {MARKET_CELO_USD, Cost::fromDouble(1E6)},
    {MARKET_KSM_USD, Cost::fromDouble(1E8)},
    {MARKET_BAL_USD, Cost::fromDouble(1E7)},
    {MARKET_BNT_USD, Cost::fromDouble(1E6)},
    {MARKET_MIR_USD, Cost::fromDouble(1E6)},
    {MARKET_SRM_USD, Cost::fromDouble(1E6)},
    {MARKET_LON_USD, Cost::fromDouble(1E6)},
    {MARKET_DODO_USD, Cost::fromDouble(1E6)},
    {MARKET_ALPHA_USD, Cost::fromDouble(1E5)},
    {MARKET_WNXM_USD, Cost::fromDouble(1E7)},
    {MARKET_XCH_USD, Cost::fromDouble(1E8)}};

const std::unordered_map<Trading::NativeSymbol, BigInt> gAssetIdMap{
    {MARKET_BTC_USD, BigInt::fromHexString("4254432d3130000000000000000000")},
    {MARKET_ETH_USD, BigInt::fromHexString("4554482d3900000000000000000000")},
    {MARKET_LINK_USD, BigInt::fromHexString("4c494e4b2d37000000000000000000")},
    {MARKET_AAVE_USD, BigInt::fromHexString("414156452d38000000000000000000")},
    {MARKET_UNI_USD, BigInt::fromHexString("554e492d3700000000000000000000")},
    {MARKET_SUSHI_USD, BigInt::fromHexString("53555348492d370000000000000000")},
    {MARKET_SOL_USD, BigInt::fromHexString("534f4c2d3700000000000000000000")},
    {MARKET_YFI_USD, BigInt::fromHexString("5946492d3130000000000000000000")},
    {MARKET_1INCH_USD, BigInt::fromHexString("31494e43482d370000000000000000")},
    {MARKET_AVAX_USD, BigInt::fromHexString("415641582d37000000000000000000")},
    {MARKET_SNX_USD, BigInt::fromHexString("534e582d3700000000000000000000")},
    {MARKET_CRV_USD, BigInt::fromHexString("4352562d3600000000000000000000")},
    {MARKET_UMA_USD, BigInt::fromHexString("554d412d3700000000000000000000")},
    {MARKET_DOT_USD, BigInt::fromHexString("444f542d3700000000000000000000")},
    {MARKET_DOGE_USD, BigInt::fromHexString("444f47452d35000000000000000000")},
    {MARKET_MATIC_USD, BigInt::fromHexString("4d415449432d360000000000000000")},
    {MARKET_MKR_USD, BigInt::fromHexString("4d4b522d3900000000000000000000")},
    {MARKET_FIL_USD, BigInt::fromHexString("46494c2d3700000000000000000000")},
    {MARKET_ADA_USD, BigInt::fromHexString("4144412d3600000000000000000000")},
    {MARKET_ATOM_USD, BigInt::fromHexString("41544f4d2d37000000000000000000")},
    {MARKET_COMP_USD, BigInt::fromHexString("434f4d502d38000000000000000000")},
    {MARKET_BCH_USD, BigInt::fromHexString("4243482d3800000000000000000000")},
    {MARKET_LTC_USD, BigInt::fromHexString("4c54432d3800000000000000000000")},
    {MARKET_EOS_USD, BigInt::fromHexString("454f532d3600000000000000000000")},
    {MARKET_ALGO_USD, BigInt::fromHexString("414c474f2d36000000000000000000")},
    {MARKET_ZRX_USD, BigInt::fromHexString("5a52582d3600000000000000000000")},
    {MARKET_XMR_USD, BigInt::fromHexString("584d522d3800000000000000000000")},
    {MARKET_ZEC_USD, BigInt::fromHexString("5a45432d3800000000000000000000")},
    {MARKET_ENJ_USD, BigInt::fromHexString("454e4a2d3600000000000000000000")},
    {MARKET_XLM_USD, BigInt::fromHexString("584c4d2d3500000000000000000000")},
    {MARKET_ETC_USD, BigInt::fromHexString("4554432d3700000000000000000000")},
    {MARKET_NEAR_USD, BigInt::fromHexString("4e4541522d36000000000000000000")},
    {MARKET_LUNA_USD, BigInt::fromHexString("4c554e412d36000000000000000000")},
    {MARKET_RUNE_USD, BigInt::fromHexString("52554e452d36000000000000000000")},
    {MARKET_TRX_USD, BigInt::fromHexString("5452582d3400000000000000000000")},
    {MARKET_XTZ_USD, BigInt::fromHexString("58545a2d3600000000000000000000")},
    {MARKET_HNT_USD, BigInt::fromHexString("484e542d3700000000000000000000")},
    {MARKET_ICP_USD, BigInt::fromHexString("4943502d3700000000000000000000")},
    {MARKET_AR_USD, BigInt::fromHexString("41522d370000000000000000000000")},
    {MARKET_FLOW_USD, BigInt::fromHexString("464c4f572d37000000000000000000")},
    {MARKET_PERP_USD, BigInt::fromHexString("504552502d36000000000000000000")},
    {MARKET_REN_USD, BigInt::fromHexString("52454e2d3500000000000000000000")},
    {MARKET_CELO_USD, BigInt::fromHexString("43454c4f2d36000000000000000000")},
    {MARKET_KSM_USD, BigInt::fromHexString("4b534d2d3800000000000000000000")},
    {MARKET_BAL_USD, BigInt::fromHexString("42414c2d3700000000000000000000")},
    {MARKET_BNT_USD, BigInt::fromHexString("424e542d3600000000000000000000")},
    {MARKET_MIR_USD, BigInt::fromHexString("4d49522d3600000000000000000000")},
    {MARKET_SRM_USD, BigInt::fromHexString("53524d2d3600000000000000000000")},
    {MARKET_LON_USD, BigInt::fromHexString("4c4f4e2d3600000000000000000000")},
    {MARKET_DODO_USD, BigInt::fromHexString("444f444f2d36000000000000000000")},
    {MARKET_ALPHA_USD, BigInt::fromHexString("414c5048412d350000000000000000")},
    {MARKET_WNXM_USD, BigInt::fromHexString("574e584d2d37000000000000000000")},
    {MARKET_XCH_USD, BigInt::fromHexString("5843482d3800000000000000000000")}};

const auto MAINNET_COLLATERAL_ASSET_ID = BigInt::fromHexString(
    "02893294412a4c8f915f75892b395ebbf6859ec246ec365c3b1f56f47c3a0a5d");
const auto ROPSTEN_COLLATERAL_ASSET_ID = BigInt::fromHexString(
    "02c04d8b650f44092278a7cb1e1028c82025dff622db96c934b611b84cc8de5a");

constexpr uint64_t LIMIT_ORDER_TYPE = 3;

constexpr int AMOUNT_FIELD_BITS = 64;
constexpr int NONCE_FIELD_BITS = 32;

constexpr int VAULT_ID_FIELD_BITS = 64;
constexpr int TIMESTAMP_FIELD_BITS = 32;
constexpr int PADDING_FIELD_BITS = 17;

constexpr uint64_t NONCE_UPPER_BOUND_EXCLUSIVE = 0x100000000;

// seven days
constexpr uint64_t ORDER_SIGNATURE_EXPIRATION_BUFFER_HOURS = 24 * 7;

} // namespace Kenetic::EsV2::Dydx::Stark

