A cpp lib computes the pedersen hash of a dYdX order and signs it by the starkex private key

Entry function:

```
Auth::sign() in auth.cc
```

**DO NOT COMPILE** at the moment because of some internal types when it's extracted from a big repo.
